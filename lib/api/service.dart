import 'dart:developer';

import 'package:api_example/api/api.dart';
import 'package:api_example/util/character.dart';
import 'package:http/http.dart' as http;

class ApiService {
  Future<List<Character>?> getCharacters() async {
    try {
      var client = http.Client();

      var uri = Uri.parse("https://rickandmortyapi.com/api/character");
      var response = await client.get(uri);
      log(response.statusCode.toString());
      if (response.statusCode == 200) {
        var json = response.body;
        return characterFromJson(json).results;
      }
    } catch (e) {
      log(e.toString());
    }
    return null;
  }
}
