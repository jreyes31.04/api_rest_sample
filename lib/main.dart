import 'package:api_example/util/color_palette.dart';
import 'package:api_example/views/home_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Api example',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: MyColorPalette.primaryGreen,
          secondary: MyColorPalette.secondaryGreen,
          tertiary: MyColorPalette.tertiaryGreen,
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(title: 'Flutter API REST sample'),
    );
  }
}
