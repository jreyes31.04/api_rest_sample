import 'package:api_example/api/service.dart';
import 'package:api_example/util/character.dart';
import 'package:api_example/util/color_palette.dart';
import 'package:api_example/views/detail_page.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Character>? _characters = [];
  bool _isLoaded = false;

  getData() async {
    _characters = await ApiService().getCharacters();

    if (_characters != null) {
      setState(() {
        _isLoaded = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Visibility(
          visible: _isLoaded,
          replacement: const CircularProgressIndicator(),
          child: GridView.count(
            crossAxisCount: screenWidth > 600 ? 4 : 2,
            crossAxisSpacing: screenWidth * 0.05,
            mainAxisSpacing: screenHeight * 0.02,
            padding: EdgeInsets.all(screenHeight * 0.02),
            children: _characters!
                .map(
                  (element) => GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext build) {
                            return DetailPage(
                              character: element,
                            );
                          },
                        ),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: MyColorPalette.primaryGreen,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image(
                            image: NetworkImage(element.image),
                            height: screenHeight * 0.09,
                          ),
                          Text(
                            element.name,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: MyColorPalette.primaryWhite,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }
}
