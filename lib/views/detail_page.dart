import 'package:api_example/util/character.dart';
import 'package:api_example/util/color_palette.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({
    Key? key,
    required this.character,
  }) : super(key: key);

  final Character character;

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  double sizeFactor = 0.1;

  Widget dataRow(String primText, String secText) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: sizeFactor * 0.08),
      child: Row(
        children: [
          SizedBox(
            width: sizeFactor * 0.33,
            child: Text(
              primText,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: MyColorPalette.primaryOrange,
                fontSize: sizeFactor * 0.04,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          const Spacer(flex: 1),
          SizedBox(
            width: sizeFactor * 0.33,
            child: Center(
              child: Text(
                secText,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: MyColorPalette.tertiaryGreen,
                  fontSize: sizeFactor * 0.04,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    sizeFactor = screenHeight - screenWidth;
    if (sizeFactor < 200) sizeFactor = screenHeight;
    return Scaffold(
      backgroundColor: MyColorPalette.primaryGreen,
      body: Center(
          child: Column(
        children: [
          SizedBox(height: sizeFactor * 0.02),
          Container(
            decoration: BoxDecoration(
              color: MyColorPalette.tertiaryGreen,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: EdgeInsets.all(sizeFactor * 0.05),
            child: Image(
              image: NetworkImage(widget.character.image),
              height: sizeFactor * 0.35,
            ),
          ),
          SizedBox(height: sizeFactor * 0.02),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  const Divider(),
                  SizedBox(height: screenHeight * 0.02),
                  dataRow("ID", widget.character.id.toString()),
                  SizedBox(height: screenHeight * 0.02),
                  dataRow("Nombre", widget.character.name),
                  SizedBox(height: screenHeight * 0.02),
                  dataRow("Estado", widget.character.status),
                  SizedBox(height: screenHeight * 0.02),
                  dataRow("Especie", widget.character.species),
                  SizedBox(height: screenHeight * 0.02),
                  dataRow(
                      "Episodios", widget.character.episode.length.toString()),
                  SizedBox(height: screenHeight * 0.02),
                  dataRow("Género", widget.character.gender),
                  SizedBox(height: screenHeight * 0.02),
                  const Divider(),
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }
}
