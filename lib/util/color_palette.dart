import 'package:flutter/material.dart';

class MyColorPalette {
  static Color primaryGreen = const Color(0xFF054035);
  static Color secondaryGreen = const Color(0xFF1C8C78);
  static Color tertiaryGreen = const Color(0xFF29F2A9);
  static Color primaryOrange = const Color(0xFFF27127);
  static Color primaryWhite = const Color(0xFFF2F2F2);
}
